$(document).ready(function() {
  var nav = $('.nav'),
      sticky = $('.hero-screen').outerHeight(),
      menuButton = $('.menu-btn');

  if ( $(window).width() > 1023 ) {
      window.onscroll = function() {myFunction()};

      function myFunction() {
          if (window.pageYOffset >= sticky) {
              nav.addClass('mobile');
              menuButton.addClass('mobile');
          } else {
              nav.removeClass('mobile');
              menuButton.removeClass('mobile');
          }
      }
  }

  menuButton.on('click', function() {
    $(this).toggleClass('menu-btn--active');
      nav.toggleClass('visible');
  });

    $('.js-show-popup').on('click', function(e) {
        e.preventDefault();
        $('html, body').css('overflow', 'hidden');
        $('.pop-up-wrapper').fadeIn();
    });

    $('.close-button').on('click', function() {
        $('html, body').css('overflow', 'auto');
        $('.pop-up-wrapper').fadeOut();
    });

  $('.nav__link').on('click', function(e) {
    e.preventDefault();

    $('html, body')
      .stop()
      .animate(
        {
          scrollTop:
            $(
              $(this)
                .attr('href')
                .replace('#', '.')
            ).offset().top
        },
        700
      );

    if (menuButton.hasClass('menu-btn--active')) {
      menuButton.toggleClass('menu-btn--active');
        nav.toggleClass('visible');
    }
  });


    $('.news-slider').slick({
        // dots: true,
        dots: false,
        arrows: false,
        infinite: false,
        autoplay: false,
        // nextArrow: $('.news .right'),
        // prevArrow: $('.news .left')
    });

    $('.main-slider').slick({
        dots: true,
        infinite: false,
        autoplay: false,
        nextArrow: $('.main-slider-wrapper .right'),
        prevArrow: $('.main-slider-wrapper .left'),
        responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        dots: false
                }
            }],
        customPaging : function(slider, i) {
            var slide;
            switch (i) {
                case 0 : slide = 'Resort';
                break;

                case 1 : slide = 'LUXURY ACCOMMODATIONS';
                break;

                case 2 : slide = 'Sustainability';
                break;

                case 3 : slide = 'LEED Certified';
                break;


            }
            return '<a class="dot">'+slide+'</a>';
        }
    });
});


//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//_______________________GOOGLE MAP_______________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________

function initMap() {
    var uluru = { lat: 38.90012618527109, lng: -119.61771356523434 };
    var map = new google.maps.Map(document.querySelector('.map'), {
        zoom: 10,
        disableDefaultUI: true,
        zoomControl: false,
        center: uluru,
        disableDoubleClickZoom: true,
        styles: [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#f5f5f5"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dadada"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e5e5e5"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#eeeeee"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#c9c9c9"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            }
        ]

    });

    var image = {
        url: 'img/promo-edgwood-04.png',
        size: new google.maps.Size(60, 60),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 60)
    };

    new google.maps.Marker({
        position: { lat: 38.966614, lng: -119.948749 },
        map: map,
        icon: image
    });
}

//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//_______________________HTML5___LOGO_____________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________


var canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;
function init() {
    canvas = document.getElementById("canvas");
    anim_container = document.getElementById("animation_container");
    dom_overlay_container = document.getElementById("dom_overlay_container");
    var comp=AdobeAn.getComposition("C9A9E10B8FC56F4DB1039670BC8EC874");
    var lib=comp.getLibrary();
    handleComplete({},comp);
    // setTimeout(function(){
    var videoContainer = $(".bg-video");
    videoContainer.attr('src', videoContainer.data('src'));
    // },500)
}
function handleComplete(evt,comp) {
    //This function is always called, irrespective of the content. You can use the variable "stage" after it is created in token create_stage.
    var lib=comp.getLibrary();
    var ss=comp.getSpriteSheet();
    exportRoot = new lib.logoanimation();
    stage = new lib.Stage(canvas);
    //Registers the "tick" event listener.
    fnStartAnimation = function() {
        stage.addChild(exportRoot);
        createjs.Ticker.setFPS(lib.properties.fps);
        createjs.Ticker.addEventListener("tick", stage);
    };
    //Code to support hidpi screens and responsive scaling.
    function makeResponsive(isResp, respDim, isScale, scaleType) {
        var lastW, lastH, lastS=1;
        window.addEventListener('resize', resizeCanvas);
        resizeCanvas();
        function resizeCanvas() {
            var w = lib.properties.width, h = lib.properties.height;
            var iw = window.innerWidth, ih=window.innerHeight;
            var pRatio = window.devicePixelRatio || 1, xRatio=iw/w, yRatio=ih/h, sRatio=1;
            if(isResp) {
                if((respDim=='width'&&lastW==iw) || (respDim=='height'&&lastH==ih)) {
                    sRatio = lastS;
                }
                else if(!isScale) {
                    if(iw<w || ih<h)
                        sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==1) {
                    sRatio = Math.min(xRatio, yRatio);
                }
                else if(scaleType==2) {
                    sRatio = Math.max(xRatio, yRatio);
                }
            }
            canvas.width = w*pRatio*sRatio;
            canvas.height = h*pRatio*sRatio;
            canvas.style.width = dom_overlay_container.style.width = anim_container.style.width =  w*sRatio+'px';
            canvas.style.height = anim_container.style.height = dom_overlay_container.style.height = h*sRatio+'px';
            stage.scaleX = pRatio*sRatio;
            stage.scaleY = pRatio*sRatio;
            lastW = iw; lastH = ih; lastS = sRatio;
            stage.tickOnUpdate = false;
            stage.update();
            stage.tickOnUpdate = true;
        }
    }
    makeResponsive(true,'both',false,1);
    AdobeAn.compositionLoaded(lib.properties.id);
    fnStartAnimation();
}

//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//_______________________INSTAGRAM____FEED________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
var loadButton = $('.js-load-more'),
    gridElem = $('#instafeed');
var userFeed = new Instafeed({
    get: 'user',
    userId: '237489135',
    clientId: '61dd5aa33cd84c97a1b31f6d9ef43572',
    accessToken: '237489135.61dd5aa.e0bf3f3075d84462963f582747668162',
    resolution: 'standard_resolution',
    template: '<a href="{{link}}" target="_blank" id="{{id}}" class="grid-item"><img src="{{image}}" /></a>',
    sortBy: 'most-recent',
    limit: 7,
    links: false,
    after: function () {
        if (!this.hasNext()) {
            loadButton.fadeOut();
        }
        if ( gridElem.data("masonry") == undefined ) {
            msnry = new Masonry('#instafeed');
            imagesLoaded( gridElem ).on( 'progress', function() {
                msnry.layout();
            });
        } else {
            msnry.reloadItems();
        }
    }
});

userFeed.run();

// bind the load more button
loadButton.on('click', function(e) {
    e.preventDefault();
    userFeed.next();
});




//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//_______________________PARALLAX_________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
//________________________________________________________________________________________________________________________________
var duration = 0, offset = 0;

if ( $(window).width > 1440 ) {
    duration = 1000;
    offset = -600;
} else if ( $(window).width > 1200 && $(window).width < 1441 ) {
    duration = 700;
    offset = -300;
} else if ( $(window).width > 1024 && $(window).width < 1201 ) {
    duration = 400;
    offset = 0;
}
var controller = new ScrollMagic.Controller();
var tween = new TimelineMax ()
    .add([
        TweenMax.to("#parallaxContainer .top", 1, {bottom: "-10", ease: Linear.easeNone}),
        TweenMax.to("#parallaxContainer .mid", 1, {bottom: "0", ease: Linear.easeNone}),
        TweenMax.to("#parallaxContainer .bottom", 1, {bottom: "0", ease: Linear.easeNone})
    ]);

// build scene
var scene = new ScrollMagic.Scene({triggerElement: "#parallaxContainer", duration: duration, offset: offset})
    .setTween(tween)
    .addTo(controller);

